<?php

namespace App\Http\Controllers;

use App\Models\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RealtorListingController extends Controller
{

    public function __construct() 
    {
        $this->authorizeResource(Listing::class, 'listing');
    }

    public function index(Request $request)
    {
        // dd( $request->boolean('deleted') );
        // console.log()
        $filters = [
            'deleted' => $request->boolean('deleted'),
            ...$request->only('by', 'order')
            // 'search' => $request->input('search'),
            // 'sort' => $request->input('sort'),
            // 'order' => $request->input('order'),
        ];
        return inertia(
            'Realtor/Index', 
            [ 
                'listings' => 
                    Auth::user()
                    ->listings()
                    ->filter($filters)
                    ->withCount('images')
                    ->paginate(8)
                    ->withQueryString(),
                'filters' => $filters
            ]
        );
    }

        /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia( 'Realtor/Create' );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->user()->listings()->create(
            $request->validate([
                'bedrooms' => ['required', 'numeric', 'min:1', 'max:20'],
                'baths' => ['required', 'numeric'],
                'area' => ['required', 'numeric'],
                'city' => ['required', 'string'],
                'code' => ['required', 'string'],
                'street' => ['required', 'string'],
                'street_number' => ['required'],
                'price' => ['required', 'numeric']
            ])
        );

        // dd($request->all());
        return redirect()->route('realtor.listing.index')
            ->with('success', 'Listing created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Listing $listing)
    {
        return inertia(
            'Realtor/Edit',
            [
                'listing' => $listing
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Listing $listing)
    {
        $listing->update(
            $request->validate([
                'bedrooms' => 'required|integer|min:0|max:20',
                'baths' => 'required|integer|min:0|max:20',
                'area' => 'required|integer|min:15|max:1500',
                'city' => 'required',
                'code' => 'required',
                'street' => 'required',
                'street_number' => 'required|min:1|max:1000',
                'price' => 'required|integer|min:1|max:20000000',
            ])
        );

        return redirect()->route('realtor.listing.index')
            ->with('success', 'Listing was changed!');
    }

    
    public function destroy(Listing $listing)
    {
        $listing->deleteOrFail();

        return redirect()->route('realtor.listing.index')
            ->with('success', 'Listing was deleted!');
    }

    public function restore( Listing $listing )
    {
        $listing->restore();

        return redirect()->route('realtor.listing.index')
            ->with('success', 'Listing was restored!');
    }
}
