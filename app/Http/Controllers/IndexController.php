<?php

namespace App\Http\Controllers;

use App\Models\Listing;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        // dd( Listing::where('bedrooms', '>', 3)->orderBy('price', 'asc')->first() );
        // dd(Auth::user());
        // $user = User::find(1);
        // $listings = $user->listings()->where('bedrooms', '>', 3)->get();
        // dd($listings);
        return inertia(
            'Index/Index',
            [
                'name' => 'John Doe',
                'age' => 30,
                'hobbies' => ['Reading', 'Coding', 'Gaming'],
            ]
        );
    }
    
    public function show()
    {
        return inertia('Index/Show');
        
    }
}
