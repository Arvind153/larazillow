<?php

namespace App\Http\Controllers;

use App\Models\Listing;
use App\Models\Offer;
use Illuminate\Http\Request;

class ListingOfferController extends Controller
{
    function store(Listing $listing, Request $request)
    {
        
        $listing->offers()->save(
            Offer::make(
                $request->validate([
                    'amount' => ['required', 'integer', 'min:1', 'max:1000000000'],
                ])
            )->bidder()->associate($request->user())
        );

        // $request->user()->offers()->create(
        //     $request->only('price', 'deposit', 'closing', 'financing', 'conditions', 'listing_id')
        // );

        return back()->with('success', 'Offer submitted successfully');
    }
}
