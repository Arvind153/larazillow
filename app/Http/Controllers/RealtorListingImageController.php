<?php

namespace App\Http\Controllers;

use App\Models\Listing;
use App\Models\ListingImage;
use Illuminate\Http\Request;
use Storage;

class RealtorListingImageController extends Controller
{
    public function create(Listing $listing)
    {
        $listing->load(['images']);
        return inertia(
            'Realtor/ListingImage/Create',
            ['listing' => $listing]
        );
    }

    public function store(Listing $listing, Request $request)
    {
        $request->validate([
            'images' => 'required|array',
            'images.*' => 'required|image'
        ],
        [
            'images.required' => 'Please upload at least one image.',
            'images.*.required' => 'Please upload at least one image.',
            'images.*.image' => 'Please upload a valid image.'
        ]);

        foreach($request->file('images') as $file ) {
            $path = $file->store('images', 'public');

            $listing->images()->save( new ListingImage([
                'filename' => $path
            ]));
        }


        return redirect()->route('listing.show', $listing)
            ->with('success', 'Listing images uploaded.');
    }

    public function destroy( ListingImage $image ) 
    {
        Storage::disk('public')->delete($image->filename);
        $image->delete();
        return redirect()->back()->with('success', 'Listing image deleted.');
    }
}
